package config

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/apex/log"
	"gopkg.in/yaml.v2"
	"io"
	"io/ioutil"
	"os"
	"path"
	"reflect"
	"strings"
	"time"
)

//Config generalized interface
type Config interface {
	Load(cfg interface{}, files ...string) (err error)
}

//Configuration is the configuration to load configurations
type Configuration struct {
	Environment          string
	Prefix               string
	LogLevel             string
	ErrorOnUnmatchedKeys bool
}

//New creates a new Configuration capability
func New(configuration *Configuration) *Configuration {
	log.SetLevelFromString("debug")
	if configuration == nil {
		configuration = &Configuration{}
	}

	if configuration.Prefix == "" {
		if prefix := os.Getenv("CONFIG_PREFIX"); prefix != "" {
			configuration.Prefix = strings.ToUpper(prefix)
		} else {
			configuration.Prefix = "CONFIG"
		}
	}

	log.Debugf("Current LogLevel: %s", configuration.LogLevel)
	if configuration.LogLevel == "" {
		if loglvl := os.Getenv(fmt.Sprintf("%s_LOGLEVEL", configuration.Prefix)); loglvl != "" {
			log.Trace(fmt.Sprintf("Setting LogLevel to %s", loglvl))
			configuration.LogLevel = strings.ToLower(loglvl)
		} else {
			log.Trace("Setting LogLevel to 'info'")
			configuration.LogLevel = "info"
		}
	} else {
		log.Trace(fmt.Sprintf("Setting LogLevel to '%s'", configuration.LogLevel))
	}
	if env := os.Getenv(fmt.Sprintf("%s_ENV", configuration.Prefix)); env != "" {
		configuration.Environment = env
	} else {
		configuration.Environment = "development"
	}

	log.SetLevelFromString(configuration.LogLevel)
	return configuration
}

//Files gives a list of files that are found based on the Configuration
//settings and a map of the times based on the filename
func (c *Configuration) Files(files ...string) ([]string, map[string]time.Time) {
	var resultKeys []string
	var results = map[string]time.Time{}

	log.Infof("Current environment: '%v'", c.Environment)
	log.Debugf("Looking through files: %v", files)

	for i := len(files) - 1; i >= 0; i-- {
		file := files[i]

		// check configuration
		if fileInfo, err := os.Stat(file); err == nil && fileInfo.Mode().IsRegular() {
			resultKeys = append(resultKeys, file)
			results[file] = fileInfo.ModTime()
		}

		// check configuration with env
		if file, modTime, err := getFileWithPrefix(file, c.Environment); err == nil {
			resultKeys = append(resultKeys, file)
			results[file] = modTime
		}
	}
	return resultKeys, results
}

//Load the configuration files into the config interface
func (c *Configuration) Load(config interface{}, files ...string) (err error) {
	defaultValue := reflect.Indirect(reflect.ValueOf(config))
	if !defaultValue.CanAddr() {
		return fmt.Errorf("Config %v should be addressable", config)
	}
	err = c.load(config, files...)

	return
}

func (c *Configuration) load(config interface{}, files ...string) (err error) {
	defer func() {
		if err != nil {
			log.Errorf("Failed to load configuration from %v, got %v\n", files, err)
		}
		log.Debugf("Configuration:\n%#v\n", c)
		return
	}()

	files, filesModTimeMap := c.Files(files...)
	log.Trace(fmt.Sprintf("Files Found: %v", files))
	for _, file := range files {
		log.Infof("Loading configurations from file '%v' modified on %v...\n", file, filesModTimeMap[file])
		if err = processFile(config, file, c.ErrorOnUnmatchedKeys); err != nil {
			return err
		}
	}
	return
}

func getFileWithPrefix(file, env string) (string, time.Time, error) {
	var (
		envFile string
		ext     = path.Ext(file)
	)

	if ext == "" {
		envFile = fmt.Sprintf("%v.%v", file, env)
	} else {
		envFile = fmt.Sprintf("%v.%v%v", strings.TrimSuffix(file, ext), env, ext)
	}
	if fileInfo, err := os.Stat(envFile); err == nil && fileInfo.Mode().IsRegular() {
		return envFile, fileInfo.ModTime(), nil
	}
	return "", time.Now(), fmt.Errorf("failed to find file %v", file)
}

func processFile(config interface{}, file string, errorOnUnmatchedKeys bool) error {
	data, err := ioutil.ReadFile(file)
	if err != nil {
		return err
	}

	switch {
	case strings.HasSuffix(file, ".yaml") || strings.HasSuffix(file, ".yml"):
		if errorOnUnmatchedKeys {
			return yaml.UnmarshalStrict(data, config)
		}
		return yaml.Unmarshal(data, config)
	case strings.HasSuffix(file, ".json"):
		return unmarshalJSON(data, config, errorOnUnmatchedKeys)
	default:

		if err := unmarshalJSON(data, config, errorOnUnmatchedKeys); err == nil {
			return nil
		} else if strings.Contains(err.Error(), "json: unknown field") {
			return err
		}

		var yamlError error
		if errorOnUnmatchedKeys {
			yamlError = yaml.UnmarshalStrict(data, config)
		} else {
			yamlError = yaml.Unmarshal(data, config)
		}

		if yamlError == nil {
			return nil
		} else if yErr, ok := yamlError.(*yaml.TypeError); ok {
			return yErr
		}

		return errors.New("failed to decode config")
	}
}

// unmarshalJSON unmarshals the given data into the config interface.
// If the errorOnUnmatchedKeys boolean is true, an error will be returned if there
// are keys in the data that do not match fields in the config interface.
func unmarshalJSON(data []byte, config interface{}, errorOnUnmatchedKeys bool) error {
	reader := strings.NewReader(string(data))
	decoder := json.NewDecoder(reader)

	if errorOnUnmatchedKeys {
		decoder.DisallowUnknownFields()
	}

	err := decoder.Decode(config)
	if err != nil && err != io.EOF {
		return err
	}
	return nil
}
