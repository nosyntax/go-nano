package application

import (
	"github.com/go-chi/chi"
	"gitlab.com/nosyntax/go-nano/pkg/filesystem/assetfs"
	"gitlab.com/nosyntax/go-nano/pkg/http/middleware"
	"gitlab.com/nosyntax/go-nano/pkg/http/router/wildcard"
	"net/http"
)

type Application interface {
	Configure(*NanoApplication)
}

type NanoApplication struct {
	*Config
}

type Config struct {
	Router   *chi.Mux
	Handlers []http.Handler
	AssetFS  assetfs.Interface
}

func New(config *Config) *NanoApplication {
	if config == nil {
		config = &Config{}
	}

	if config.Router == nil {
		config.Router = chi.NewRouter()
	}

	return &NanoApplication{Config: config}
}

//Use mount the router into the nano-app
func (nano *NanoApplication) Use(app Application) {
	app.Configure(nano)
}

func (nano *NanoApplication) NewServeMux() http.Handler {
	var stack *middleware.Stack = middleware.NewStack()
	if len(nano.Handlers) > 0 {
		wildcard := wildcard.New()
		for _, handler := range nano.Config.Handlers {
			wildcard.AddHandler(handler)
		}
		wildcard.AddHandler(nano.Config.Router)

		stack.Apply(wildcard)
	}

	return stack.Apply(nano.Config.Router)
}
