package middleware

import (
	"fmt"
	"net/http"
	"strings"
)

//Middleware is the core structure of a middleware
type Middleware struct {
	Name                    string
	Handler                 func(http.Handler) http.Handler
	After, Before, Requires []string
}

//Stack is a stack, "list", of middlwares
type Stack struct {
	middlewares []*Middleware
}

//NewStack creates a new Stack
func NewStack() *Stack {
	return &Stack{}
}

//Apply adds a http.Handler to the current Stack
func (stack *Stack) Apply(handler http.Handler) http.Handler {
	var (
		retHandler  http.Handler
		sorted, err = stack.sort()
	)

	if err != nil {
		fmt.Println(err)
	}

	for idx := len(sorted) - 1; idx >= 0; idx-- {
		middleware := sorted[idx]
		if retHandler == nil {
			retHandler = middleware.Handler(handler)
		} else {
			retHandler = middleware.Handler(retHandler)
		}
	}

	return retHandler
}

//String returns the string representation of the stack
func (stack *Stack) String() string {
	var sortedNames []string
	if sorted, err := stack.sort(); err != nil {
		fmt.Println(err)
	} else {
		for _, middleware := range sorted {
			sortedNames = append(sortedNames, middleware.Name)
		}
	}
	return fmt.Sprintf("Middleware Stack: %v", strings.Join(sortedNames, ", "))
}

//RemoveByName removes the middleware from the stack via its Name
func (stack *Stack) RemoveByName(name string) {
	currentMiddlewares := stack.middlewares
	for idx, middleware := range currentMiddlewares {
		if middleware.Name == name {
			if idx > 0 {
				stack.middlewares = stack.middlewares[0 : idx-1]
			} else {
				stack.middlewares = []*Middleware{}
			}

			if idx < len(currentMiddlewares)-1 {
				stack.middlewares = append(stack.middlewares, currentMiddlewares[idx+1:]...)
			}
		}
	}
}

//Use appends a middleware to the stack
func (stack *Stack) Use(middleware Middleware) {
	stack.middlewares = append(stack.middlewares, &middleware)
}

func (stack *Stack) sort() (sorted []*Middleware, err error) {
	var (
		errors             []error
		names, sortedNames []string
		collection         = map[string]*Middleware{}
		sortCollection     func(mw *Middleware)
	)

	for _, middleware := range stack.middlewares {
		collection[middleware.Name] = middleware
		names = append(names, middleware.Name)
	}

	for _, middleware := range stack.middlewares {
		for _, require := range middleware.Requires {
			if _, ok := collection[require]; !ok {
				errors = append(errors, fmt.Errorf("middleware %v requires %v but is non-existent", middleware.Name, require))
			}
		}
		for _, inBefore := range middleware.Before {
			if mw, ok := collection[inBefore]; ok {
				mw.After = uniqueAppend(mw.After, middleware.Name)
			}
		}
		for _, inAfter := range middleware.After {
			if mw, ok := collection[inAfter]; ok {
				mw.After = uniqueAppend(mw.Before, middleware.Name)
			}
		}
	}

	if len(errors) > 0 {
		return nil, fmt.Errorf("%v", errors)
	}

	sortCollection = func(mw *Middleware) {
		if _, found := reverseLookup(sortedNames, mw.Name); !found {
			var minIdx = -1
			for _, inAfter := range mw.After {
				idx, found := reverseLookup(sortedNames, inAfter)
				if !found {
					if middleware, ok := collection[inAfter]; ok {
						sortCollection(middleware)
						idx, found = reverseLookup(sortedNames, inAfter)
					}
				}
				if found && idx > minIdx {
					minIdx = idx
				}
			}
			for _, inBefore := range mw.Before {
				if idx, found := reverseLookup(sortedNames, inBefore); found {
					if idx < minIdx {
						sortedNames = append(sortedNames[:idx], sortedNames[idx+1:]...)
						sortCollection(collection[inBefore])
						return
					}
				}
			}

			if minIdx >= 0 {
				sortedNames = append(sortedNames[:minIdx+1], append([]string{mw.Name}, sortedNames[minIdx+1:]...)...)
			} else if _, has := reverseLookup(sortedNames, mw.Name); !has {
				sortedNames = append(sortedNames, mw.Name)
			}
		}
	}

	for _, middleware := range stack.middlewares {
		sortCollection(middleware)
	}
	for _, name := range sortedNames {
		sorted = append(sorted, collection[name])
	}

	return sorted, nil
}

func reverseLookup(names []string, search string) (int, bool) {
	size := len(names)
	for idx := range names {
		if names[size-1-idx] == search {
			return size - 1 - idx, true
		}
	}
	return -1, false
}

func uniqueAppend(strs []string, str string) []string {
	for _, st := range strs {
		if st == str {
			return strs
		}
	}
	return append(strs, str)
}
